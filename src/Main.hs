module Main where

-- Imports
import Control.Monad
import CricSim.Ball
import CricSim.Player
import Data.List
import System.Random.MWC

-- Main action
main :: IO ()
main = withSystemRandom . asGenIO $ \rng -> do

  -- Two players
  let kallis  = Player 45789 "Jacques Kallis" 55.37 32.65
  let mcgrath = Player 6565  "Glenn McGrath"  7.36  21.64

  -- Simulate a million deliveries
  outcomes <- replicateM 1000 (simulateBall mcgrath kallis rng)

  -- Total number of wickets and runs
  let tot = foldl'
            (\(a, b) x -> case x of
                                  Wicket -> (a, b+1)
                                  Runs n -> (a+n, b))
            (0, 0) outcomes :: (Int, Int)

  let str = show (fst tot) ++ "/" ++ show (snd tot)
  putStrLn str
  return ()

