-- Functions and types to do with players

module CricSim.Player where

-- A player type. Players have a unique ID,
-- and some parameters describing them.
data Player = Player
              {
                playerId             :: !Int,
                playerName           :: !String,
                playerBattingAbility :: !Double,
                playerBowlingAbility :: !Double
              } deriving (Eq, Read, Show)

