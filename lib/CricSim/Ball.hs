-- Stuff to simulate the outcome of a ball.

{-# LANGUAGE RecordWildCards #-}

module CricSim.Ball where

-- Imports
import Control.Monad.Primitive
import CricSim.Player
import Data.Vector.Unboxed as U
import System.Random.MWC

-- Relative probabilities for each number of runs
-- Constraint: expected number of runs per nonwicket ball = 0.5
-- Not a MaxEnt assignment ;)
runProbsUnnormalized :: U.Vector Double
runProbsUnnormalized = U.fromList [15.55, 2.0, 0.5, 0.2, 1.5, 0.0, 0.05]



-- Normalized to a *max* of 1 (not sum of 1)
runProbs :: U.Vector Double
runProbs = U.map (* c) runProbsUnnormalized
  where
    c = 1.0 / (U.sum runProbsUnnormalized)



-- Generate according to runProbs
generateRuns :: Gen RealWorld
             -> IO Int
generateRuns rng = do
  k <- uniformR (0, 6) rng :: IO Int
  u <- uniform rng         :: IO Double
  let accept = u < (runProbs U.! k)
  if accept then return k else generateRuns rng



-- Calculate the probability of a wicket
wicketProbability :: Player -> Player -> Double
wicketProbability (Player _ _ _ bowl) (Player _ _ bat _) = 1.0 / (mu + 1.0)
  where
    mu = bat * bowl / 15.0


-- Type for the outcome of a ball
data Outcome = Wicket | Runs Int


-- Simulate a ball
simulateBall :: Player
             -> Player
             -> Gen RealWorld
             -> IO Outcome
simulateBall bowler batter rng = do

  -- Print the message
  putStr $ playerName bowler
  putStr " to "
  putStr $ playerName batter
  putStr ": "

  -- Generate outcome of the ball
  let wicketProb = wicketProbability bowler batter
  u <- uniform rng :: IO Double
  outcome <- if u < wicketProb
                then return Wicket
                else fmap Runs $ generateRuns rng

  -- Finish printing the message
  case outcome of
         Wicket -> putStrLn "OUT"
         Runs 0 -> putStrLn "no run"
         Runs 1 -> putStrLn "one run"
         Runs 2 -> putStrLn "two runs"
         Runs 3 -> putStrLn "three runs"
         Runs 4 -> putStrLn "FOUR"
         Runs 6 -> putStrLn "SIX"
         Runs _ -> putStrLn "Unknown outcome"

  return outcome

